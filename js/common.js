var app = app || {};

var spBreak = 768;

app.init = function() {
    app.menu();
    app.smooth_scroll();
};

app.isMobile = function() {

    return window.matchMedia('(max-width: ' + spBreak + 'px)').matches;

};

app.isOldIE = function() {

    return $('html.ie9').length || $('html.ie10').length;

};

app.menu = function() {
    $('.drawer-hamburger').on('click', function() {
        $(this).toggleClass('active');
        if ($(this).hasClass('active')) {
            $('.sm-gnav').addClass('is-open');
        } else {
            $('.sm-gnav').removeClass('is-open');
        }
    });
};

app.smooth_scroll = function() {
     $('a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");
        
        $('a').each(function () {
            $(this).removeClass('active');
        })
        $(this).addClass('active');
      
        var target = this.hash,
            menu = target;
        $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top+2
        }, 800, );
    });
}
$(function() {

    app.init();

});
